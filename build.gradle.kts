group = "maxdistructo"
version = "1.0-SNAPSHOT"

plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.31")
}

repositories {
    mavenCentral()
    maven("https://oss.sonatype.org/content/repositories/snapshots")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    compile("no.tornado:tornadofx:1.7.19")
    compile("io.netty:netty-all:4.1.36.Final")
    compile("org.json:json:20180813")
    compile("org.jetbrains.kotlin:kotlin-reflect")
    compile("commons-io:commons-io:2.6")
    compile("org.springframework.security:spring-security-core:5.1.5.RELEASE")
    compile("ch.qos.logback:logback-classic:1.2.3")
}