/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package maxdistructo.organizer.server

import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import maxdistructo.organizer.AESUtils
import maxdistructo.organizer.network.Packets
import maxdistructo.organizer.network.Types
import maxdistructo.organizer.server.TelnetServer.idStart
import org.json.JSONObject

import java.net.InetSocketAddress
import java.util.*

/**
 * Handles a server-side channel.
 */
@Sharable
class TelnetServerHandler : SimpleChannelInboundHandler<String>() {

    val map = mutableMapOf<String, String>()

    @Throws(Exception::class)
    override fun channelActive(ctx: ChannelHandlerContext) {
        // Send greeting for a new connection
        val client = (ctx.channel().remoteAddress() as InetSocketAddress)
        TelnetServer.logger.info(client.hostName + " has connected.")
        ctx.write("Welcome " + client.address.canonicalHostName + " : " + client.address.hostAddress)
        ctx.flush()
    }

    override fun acceptInboundMessage(msg: Any?): Boolean {
        return true
    }

    @Throws(Exception::class)
    override fun channelRead(ctx: ChannelHandlerContext, requestIn: Any?) {
        val request = AESUtils.decrypt(requestIn as String)
        println("Recieved Message: $request")
        val recievedPacket = try{JSONObject(request)}catch(e : Exception){null}
        val remoteAddress = (ctx.channel().remoteAddress() as InetSocketAddress)
        if(recievedPacket != null && !recievedPacket.isEmpty) {
            when (Types.valueOf(recievedPacket.getString("type"))) {
                Types.USER -> {}
                Types.RANK -> {}
                Types.LOGIN -> {
                    val uuid = UUID.randomUUID().toString()
                    map[remoteAddress.toString()] = uuid
                    Packets.LoginSuccessPacket(Packets.PacketData(idStart, Types.LOGIN_SUCCESS), uuid)}
                Types.CATEGORY -> {}
                Types.TICKET -> {}
                Types.LOGIN_SUCCESS -> TelnetServer.logger.info("Received invalid Packet from ${remoteAddress.address}. Please try again.")
            }
        }
        else{
            TelnetServer.logger.info("Received invalid Packet from ${remoteAddress.address}. Please try again.")
        }
    }

    @Throws(Exception::class)
    public override fun channelRead0(ctx: ChannelHandlerContext, request: String) {}

    override fun channelReadComplete(ctx: ChannelHandlerContext) {
        ctx.flush()
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        cause.printStackTrace()
        ctx.close()
    }
}

