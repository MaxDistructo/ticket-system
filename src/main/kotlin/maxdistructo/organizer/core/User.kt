package maxdistructo.organizer.core

import maxdistructo.organizer.network.Packets
import maxdistructo.organizer.network.PacketRequests.requestRank
import maxdistructo.organizer.network.Types
import org.json.JSONArray
import java.net.URL

class User(packetIn : Packets.UserPacket) : PacketObject(packetIn){
  override val type = Types.USER
  val name = packetIn.name
  val rank = Rank(requestRank(packetIn.rank))
  val profilePhoto = URL(packetIn.profilePhoto)
  val perms = convertPacketToPerms(packetIn.globalPerms)
  private fun convertPacketToPerms(jsonArray : JSONArray) : List<Enum<Permissions>>{
    val out = mutableListOf<Enum<Permissions>>()
    for(value in jsonArray){
      
    }
    return out
  }
}
