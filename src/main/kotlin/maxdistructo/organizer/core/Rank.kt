package maxdistructo.organizer.core

import maxdistructo.organizer.network.Packets
import maxdistructo.organizer.network.Types
import java.awt.Color

class Rank(packetIn : Packets.RankPacket) : PacketObject(packetIn) {
    override val type = Types.RANK
    val name = packetIn.name!!
    val color = Color.decode(packetIn.color)
}