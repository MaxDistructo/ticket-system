package maxdistructo.organizer.core

import maxdistructo.organizer.network.Packets
import maxdistructo.organizer.network.Types

class Ticket (packetIn : Packets.TicketPacket) : PacketObject(packetIn) {
    override val type = Types.TICKET
    val name = packetIn.name
    val creator = packetIn.creator
    val details = packetIn.details
}