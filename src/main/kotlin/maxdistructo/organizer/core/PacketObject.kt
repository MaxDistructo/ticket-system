package maxdistructo.organizer.core

import maxdistructo.organizer.network.IPacket
import maxdistructo.organizer.network.Types

abstract class PacketObject(val packet : IPacket) {
    val id = packet.id
    val time = packet.time
    abstract val type : Enum<Types>
}