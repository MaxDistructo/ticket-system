package maxdistructo.organizer.core

import maxdistructo.organizer.network.Packets
import maxdistructo.organizer.network.PacketRequests.requestTicket
import maxdistructo.organizer.network.Types
import org.json.JSONArray

class Category(val packet : Packets.CategoryPacket){
  val id = packet.id
  val type = Types.CATEGORY
  val time = packet.time
  val name = packet.name
  val creator = packet.creator
  val tickets : List<Ticket> = requestTickets(packet.tickets)
  private fun requestTickets(jsonArray : JSONArray) : List<Ticket>{
    val out = mutableListOf<Ticket>()
    for(e in jsonArray){
      out.add(Ticket(requestTicket(e as Long)))
    }
    return out
  }
}
