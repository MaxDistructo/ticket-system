package maxdistructo.organizer.core

enum class Permissions{
  OWNER,
  GLOBAL_WRITE,
  GLOBAL_READ,
  GROUP_WRITE,
  GROUP_READ,
  CATEGORY_WRITE,
  CATEGORY_READ

}
