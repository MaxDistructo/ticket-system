package maxdistructo.organizer.api

import maxdistructo.organizer.core.Category
import maxdistructo.organizer.core.Rank
import maxdistructo.organizer.core.Ticket
import maxdistructo.organizer.core.User
import maxdistructo.organizer.network.PacketRequests



class AvaliumAPI(private val key : String) {
    fun requestUser(id : Long) : User{
        return User(PacketRequests.requestUser(id))
    }
    fun requestRank(id : Long) : Rank{
        return Rank(PacketRequests.requestRank(id))
    }
    fun requestTicket(id : Long) : Ticket {
        return Ticket(PacketRequests.requestTicket(id))
    }
    fun requestCategory(id : Long) : Category{
        return Category(PacketRequests.requestCategory(id))
    }
}