package maxdistructo.organizer

// Code was found from https://gist.github.com/SimoneStefani/99052e8ce0550eb7725ca8681e4225c5 and converted to Kotlin

import java.security.Key
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import java.util.Base64

object AESUtils {
    private val ALGO = "AES"
    private val master_key = "SecretKey101" //DO NOT USE OUTSIDE OF TESTING!!

    /**
     * Encrypt a string with AES algorithm.
     *
     * @param data is a string
     * @return the encrypted string
     */
    fun encrypt(data: String) : String{
        return encrypt(data, master_key)
    }
    @Throws(Exception::class)
    fun encrypt(data: String, keyIn : String): String {
        val key = generateKey(keyIn)
        val c = Cipher.getInstance(ALGO)
        c.init(Cipher.ENCRYPT_MODE, key)
        val encVal = c.doFinal(data.toByteArray())
        return Base64.getEncoder().encodeToString(encVal)
    }

    /**
     * Decrypt a string with AES algorithm.
     *
     * @param encryptedData is a string
     * @return the decrypted string
     */
    fun decrypt(encryptedData : String) : String{
        return decrypt(encryptedData, master_key)
    }
    @Throws(Exception::class)
    fun decrypt(encryptedData: String, keyIn : String): String {
        val key = generateKey(keyIn)
        val c = Cipher.getInstance(ALGO)
        c.init(Cipher.DECRYPT_MODE, key)
        val decordedValue = Base64.getDecoder().decode(encryptedData)
        val decValue = c.doFinal(decordedValue)
        return String(decValue)
    }

    /**
     * Generate a new encryption key.
     */
    @Throws(Exception::class)
    private fun generateKey(key : String): Key {
        return SecretKeySpec(key.toByteArray(), ALGO)
    }

}
