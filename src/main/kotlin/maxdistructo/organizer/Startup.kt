package maxdistructo.organizer

import javafx.stage.Stage
import maxdistructo.organizer.client.gui.Views
import tornadofx.App

class Startup {
    class TornadoApp : App(Views.LoginView::class){
        override fun start(stage: Stage) {
            stage.minHeight = 200.0
            stage.minWidth = 400.0
            super.start(stage)
        }
    }


    fun main(args : Array<String>){
        println("Hello World")
    }
}