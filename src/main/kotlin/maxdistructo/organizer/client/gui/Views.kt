package maxdistructo.organizer.client.gui

import javafx.geometry.Orientation
import tornadofx.*

object Views{
    class MainView : View() {
        override val root = group { label("Hello World") }
    }
    class LoginView : View() {
        override val root = group { form {
            fieldset(labelPosition = Orientation.VERTICAL) {
                field("Username") { textfield() }
                field("Password") { passwordfield() }
                button("Log in") {
                    action { println("Handle button press") }
                }
            }
        }}
    }
}