/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package maxdistructo.organizer.client.network

import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelFuture
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioSocketChannel
import io.netty.handler.ssl.SslContext
import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.util.InsecureTrustManagerFactory
import maxdistructo.organizer.network.IPacket
import maxdistructo.processcom.network.client.TelnetClientInitializer

/**
 * Simplistic telnet client.
 */
object TelnetClient {

    internal val SSL = System.getProperty("ssl") != null
    var HOST = System.getProperty("host", "172.0.0.1")
    internal val PORT = Integer.parseInt(System.getProperty("port", if (SSL) "8992" else "8023"))
    var isConnected = false
    private val todo = mutableListOf<String>()
    val requestCache : Map<String, String> = mapOf()

    fun connect(){
        main(arrayOf())
    }
    fun connect(ip : String){
        HOST = System.getProperty("host", ip)
    }

    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        // Configure SSL.
        val sslCtx: SslContext? = if (SSL) {
            SslContextBuilder.forClient()
                    .trustManager(InsecureTrustManagerFactory.INSTANCE).build()
        } else {
            null
        }

        val group = NioEventLoopGroup()
        try {
            val b = Bootstrap()
            b.group(group)
                    .channel(NioSocketChannel::class.java)
                    .handler(TelnetClientInitializer(sslCtx))

            // Start the connection attempt.
            val ch = b.connect(HOST, PORT).sync().channel()
            isConnected = true
            // Read commands from the stdin.
            var lastWriteFuture: ChannelFuture? = null
                while (isConnected) {
                   if(todo.isNotEmpty()){ //If there is a message in the queue
                        lastWriteFuture = ch.writeAndFlush(todo[0]) //Send message
                       todo.removeAt(0) //Remove message from queue.
                   }
                }

            // Wait until all messages are flushed before closing the channel.
            lastWriteFuture?.sync()
        }
        catch(e : Exception){ //Not force shutting down the client as that would cause an issue.
            e.printStackTrace()
            isConnected = false
        }
    }

    fun addToQueue(packet : IPacket){
        todo.add(packet.toEncryptedString())
    }
}
