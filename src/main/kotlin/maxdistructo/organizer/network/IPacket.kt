package maxdistructo.organizer.network

import maxdistructo.organizer.AESUtils
import org.json.JSONObject

abstract class IPacket{
  abstract val id : Long
  abstract val type : Enum<Types>
  abstract val time : Long
  abstract val subdata : JSONObject
  abstract val key : String?
  override fun toString() : String{
    val obj = JSONObject()
    obj.put("id", id)
    obj.put("type", type.name)
    obj.put("time", time)
    obj.put("subdata", subdata)
    return obj.toString()
  }
  fun toEncryptedString() : String{
    return if(this.key != null) {
      AESUtils.encrypt(this.toString(), this.key!!)
    } else{
      this.toString()
    }
  }
}
