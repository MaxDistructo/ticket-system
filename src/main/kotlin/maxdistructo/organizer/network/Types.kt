package maxdistructo.organizer.network

enum class Types(val num : Int){
  LOGIN(1),
  CATEGORY(2),
  TICKET(3),
  USER(4),
  RANK(5),
  LOGIN_SUCCESS(6)
}
