package maxdistructo.organizer.network

import maxdistructo.organizer.client.network.TelnetClient

object PacketRequests {
    fun requestRank(id : Long) : Packets.RankPacket{
        if(TelnetClient.isConnected){
            TelnetClient.addToQueue()
        }
        else{
            TelnetClient.connect()
        }
    }
    fun requestTicket(id : Long) : Packets.TicketPacket{

    }
    fun requestUser(id : Long) : Packets.UserPacket{

    }
    fun requestCategory(id : Long) : Packets.CategoryPacket{

    }
}