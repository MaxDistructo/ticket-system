package maxdistructo.organizer.network

import org.json.JSONArray
import org.json.JSONObject
import java.time.Instant

object Packets{
  data class PacketData(val id : Long, val packetType : Enum<Types>){val time = Instant.now().epochSecond}
  /**
  *
  * The default implementation of maxdistructo.organizer.network.IPacket. Most packets will extend this instead of maxdistructo.organizer.network.IPacket.
  * @class Packet
  * @param id The ID of the packet
  * @param type The Enum type of the Packet. Used on all packets.
  * @param time The time that the Object represented by this packet was created.
  * @param subdata The extra data that is specific to this object type.
  *
  */
  open class Packet (val packetData : PacketData, override val subdata: JSONObject) : IPacket() {
    override val id = packetData.id
    override val type = packetData.packetType
    override val time = packetData.time
    override val key = null

    }
  class CategoryPacket(packetDataIn : PacketData, val name : String, val creator : Long, val tickets : JSONArray) : Packet(packetDataIn, JSONObject().put("name", name).put("creator", creator).put("tickets", tickets))
  class TicketPacket(packetIn : Packet, key : String) : Packet(packetIn.packetData, packetIn.subdata){
    //override val subdata = packetIn.subdata
    val name = subdata.optString("name")
    val creator = subdata.optLong("creator")
    val details = subdata.optString("details")
  }
  class UserPacket(packetIn : Packet, key : String) : Packet(packetIn.packetData, packetIn.subdata){
    val name = subdata.optString("name")
    val globalPerms = subdata.optJSONArray("permissions")
    val rank = subdata.optLong("rank")
    val profilePhoto = subdata.optString("photo")
  }
  class RankPacket(packetIn : Packet, key : String) : Packet(packetIn.packetData, packetIn.subdata){
    val name = subdata.optString("name")
    val color = subdata.optString("color")
  }
  class LoginPacket(val packetData : PacketData, val username : String, val password : String) : IPacket() {
    override val id: Long = packetData.id
    override val subdata = JSONObject().put("username", username).put("password", password)
    override val type = packetData.packetType
    override val time = packetData.time
    override val key = null
  }
  class LoginSuccessPacket(val packetData : PacketData, override val key : String) : IPacket(){
    override val id: Long = packetData.id
    override val subdata = JSONObject().put("key", key)
    override val type = packetData.packetType
    override val time = packetData.time
  }
  class RequestPacket(override val id : Long, override val type : Enum<Types>, override val key : String) : IPacket(){
    override val subdata = JSONObject() //Empty object as it does not apply to this packet.
    override val time = Instant.now().epochSecond
  }
}
